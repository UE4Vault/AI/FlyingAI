// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
//#include "EngineUtils.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Perch_A.generated.h"


UCLASS()
class ALTFLYINGAI_API APerch_A : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APerch_A();


	// Path


	UFUNCTION(BlueprintPure, Category = "Path")
		void GetPathVectors(FVector StartLocation, FVector RelativeEndLocation ,FVector& LeftStart, FVector& LeftEnd, FVector& MiddleStart, FVector& MiddleEnd, FVector& RightStart, FVector& RightEnd);

	UFUNCTION(BlueprintCallable, Category = "Path")
		void DrawPath();

	UFUNCTION(BlueprintCallable, Category = "Path")
		void NewPath();


	// Uncategorized


	UFUNCTION(BlueprintCallable)
		void NewOccupant(AActor* NewOccupant);

	UFUNCTION(BlueprintCallable)
		void UpdateText();

	UFUNCTION(BlueprintPure)
		FVector Lookup(FVector ActorLocation);


	// Properties


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlowField")
		bool UseFlowField = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlowField")
		int Columns = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlowField")
		int Rows = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlowField")
		float Spacing = 1000;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlowField")
		float BoundsX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlowField")
		float BoundsY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path")
		bool UsePath = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path")
		float Radius = 40;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path")
		FVector RelativePathEnd = FVector(150, 150, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path")
		int PathExtensions = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path")
		TArray<FVector> CurrentPathEndPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path")
		TArray<FVector> CurrentPathExtensionsRelatives;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* Occupant = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool UseArrowRotation = true;


	// Components


	UPROPERTY(EditAnywhere, Category = "Components")
		USphereComponent* Sphere;

	UPROPERTY(EditAnywhere, Category = "Components")
		UArrowComponent* Arrow;

	UPROPERTY(EditAnywhere, Category = "Components")
		UTextRenderComponent* TextRender;

	UPROPERTY(EditAnywhere, Category = "Components")
		UStaticMeshComponent* Perch;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Called when actor is created
	virtual void OnConstruction(const FTransform& Transform);
};
