// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "GameFramework/Character.h"
#include "Perch_A.h"
#include "Flying_AC.generated.h"


//Enums


UENUM(BlueprintType)
enum class ERotationDirection : uint8
{
	RD_Clockwise 			UMETA(DisplayName = "Clockwise"),
	RD_CounterClockwise 	UMETA(DisplayName = "Counter-Clockwise"),
};


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALTFLYINGAI_API UFlying_AC : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFlying_AC();


	// Behaviours


	/*Seek or Flee a specified target*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector Seek(FVector Target, bool Flee, float Weight = 1);

	/*Arrive at a specified target within a distance*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector Arrive(FVector Target, float Distance = 300, float Weight = 1);

	/*Align velocity with other boids within a distance*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector Alignment(float MaxDistance = 500, float Weight = 1);

	/*Keep a specified distance between other boids and self*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector Separation(float MinSeparation = 250, float Weight = 1);

	/*Fly towards the average location of all boids within a certain distance*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector Cohesion(float MinDistance = 500, float Weight = 1);

	/*Follow the wingtip of another boid and align to the flow*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector AlignWingsToAirFlow(int32 Wingspan = 75, float Weight = 1);

	/*Pursue or Evade another actor using this movement component*/
	UFUNCTION(BlueprintPure, Category = "Behaviours")
		FVector Pursuit(AActor* Target, float LengthOfPrediction = 25, bool Evade = false, float Weight = 1);


	// Utilities


	/*Get Boids within a certain distance and report some stats*/
	UFUNCTION(BlueprintCallable, Category = "Utilities")
		void GetNearbyBoids(TArray<FVector>& NormalScaledByDistance, FVector& Velocities, FVector& OtherLocations, float Distance = 500);

	UFUNCTION(BlueprintCallable, Category = "Utilities")
		TArray<AActor*> GetOtherFMCBoids();

	/*Apply forces to owning actor each tick*/
	UFUNCTION(BlueprintCallable, Category = "Utilities")
		void ApplyForces(FVector Force, FVector& Change);

	UFUNCTION(BlueprintPure, Category = "Utilities")
		FVector GetNormalPoint(FVector Point, FVector LineStart, FVector LineEnd);

	UFUNCTION(BlueprintPure, Category = "Utilities")
		AActor* GetLeader();


	// Natural Forces


	/*Calculate wind or water force*/
	UFUNCTION(BlueprintPure, Category = "Natural Forces")
		FVector Current(FVector Current, float Weight = 1);

	// Combined Behaviours

	UFUNCTION(BlueprintPure, Category = "Combined Behaviours")
		FVector Flocking();

	/*Keep other birds in sight unless leader*/
	UFUNCTION(BlueprintPure, Category = "Combined Behaviours")
		FVector V_Formation();


	//Unimplemented Behaviours


	/*Follow a flow field*/
	UFUNCTION(BlueprintPure, Category = "UnimplementedBehaviours")
		FVector FollowFlowField(APerch_A* FlowField);

	/*Follow a path*/
	UFUNCTION(BlueprintPure, Category = "UnimplementedBehaviours")
		FVector FollowPath(APerch_A* Path);

	UFUNCTION(BlueprintPure, Category = "UnimplementedBehaviours")
		FVector ObstacleAvoidance(int32 MaxVisionAhead = 400, float Weight = 1);

	UFUNCTION(BlueprintPure, Category = "UnimplementedBehaviours")
		FVector Boundaries(int32 MaxVisionAhead = 400, float Weight = 1);

	UFUNCTION(BlueprintPure, Category = "UnimplementedBehaviours")
		FVector FlyInCircle(ERotationDirection MovementDirection, float Radius = 1000, float SegmentAngle = 45);


	// Properties


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxSpeed = 15;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxForce = .3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Leader = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Acceleration = FVector(0, 0, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Velocity = FVector(0, 0, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool DrawOnce = false;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
