// Fill out your copyright notice in the Description page of Project Settings.

#include "Flying_AC.h"


// Sets default values for this component's properties
UFlying_AC::UFlying_AC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	bAutoActivate = true;
	SetAutoActivate(true);
}


/*Behaviours*/


FVector UFlying_AC::Seek(FVector Target, bool Flee, float Weight)
{
	//Use actor's location to determine seek path
	FVector A = (Target - GetOwner()->GetActorLocation());
	//Normal * Max Speed = Magnitude
	A = A.GetSafeNormal() * MaxSpeed;
	if (Flee)
		A = -A;
	//Desired - Velocity | Don't exceed allowed MaxForce
	A = (A - Velocity).GetClampedToSize(0, MaxForce);
	return A = A * Weight;
}

FVector UFlying_AC::Arrive(FVector Target, float Distance, float Weight)
{
	//Desired movement path
	FVector Desired = Target - GetOwner()->GetActorLocation();
	float A = Desired.Size();
	if (A < Distance)
		Desired = Desired.GetSafeNormal() * UKismetMathLibrary::MapRangeUnclamped(A, 0, 0, 0, MaxSpeed);
	else
		Desired = Desired.GetSafeNormal() * MaxSpeed;
	return Desired - Velocity * Weight;
}

FVector UFlying_AC::Alignment(float MaxDistance, float Weight)
{
	TArray<FVector> LDistances;
	FVector LVelocities;
	FVector LOtherLocations;

	GetNearbyBoids(LDistances, LVelocities, LOtherLocations, MaxDistance);

	//Check to see if any boids are nearby
	if (LDistances.Num() > 0)
		return (((LVelocities / LDistances.Num()).GetSafeNormal() * MaxSpeed) - Velocity).GetClampedToSize(0, MaxForce) * Weight;
	else
		//Go Nowhere
		return FVector();
}

FVector UFlying_AC::Separation(float MinSeparation, float Weight)
{
	TArray<FVector> LDistances;
	FVector LVelocities;
	FVector LOtherLocations;

	GetNearbyBoids(LDistances, LVelocities, LOtherLocations, MinSeparation);

	//Check to see if any boids are nearby
	if (LDistances.Num() > 0)
	{
		//Check magnitude and if it is nothing don't go anywhere
		if (UKismetMathLibrary::GetVectorArrayAverage(LDistances).Size() > 0)
		{
			return ((UKismetMathLibrary::GetVectorArrayAverage(LDistances).GetSafeNormal() * MaxSpeed) - Velocity).GetClampedToSize(0, MaxForce) * Weight;
		}
		else
			return FVector();
	}
	else
		return FVector();
}

FVector UFlying_AC::Cohesion(float MinDistance, float Weight)
{
	TArray<FVector> LDistances;
	FVector LVelocities;
	FVector LOtherLocations;

	GetNearbyBoids(LDistances, LVelocities, LOtherLocations, MinDistance);

	//Check to see if any boids are nearby
	if (LDistances.Num() > 0)
		//Go to average of boid distances
		return Seek(LOtherLocations / LDistances.Num(), false, Weight);
	else
		return FVector();
}

FVector UFlying_AC::AlignWingsToAirFlow(int32 Wingspan, float Weight)
{
	float RecordDistance = 9999999;
	//Also known as RecordBoid
	AActor* OtherBoid = nullptr;

	if (Leader)
	{
		//This only seems to activate for the first 5 seconds or so and then stop.
		//Figure out a way to make this work you nerd
		//This code fucking makes my eyes sting, ugh
		//Oh wait, it seems to skip this completely and keep moving in one direction....
		//WTF
		//Stupid code

		//Draw Leader
		UKismetSystemLibrary::DrawDebugLine(GetWorld(), GetOwner()->GetActorLocation(), GetOwner()->GetActorLocation() + (GetOwner()->GetActorUpVector() * 100), FLinearColor::Red, .07, 15);
		//Seek player character
		FString OwnerLocation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorLocation().ToString();
		UE_LOG(LogTemp, Warning, TEXT("Location: %s"), *OwnerLocation);
		return Arrive(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorLocation(), 300, Weight);
	}
	else
	{
		TArray<AActor*> FMCBoids = GetOtherFMCBoids();
		for (AActor* ActorItr : FMCBoids)
		{
			//Subtract owner and boid
			FVector OB = ActorItr->GetActorLocation() - GetOwner()->GetActorLocation();
			//Get normal between two owner and boid
			FVector A = OB.GetSafeNormal();
			//Find angle to other boid in degrees
			float B = UKismetMathLibrary::DegAcos(FVector().DotProduct(A, GetOwner()->GetActorForwardVector()));
			//View angle on each half of vision
			if (B < 75)
			{
				if (OB.Size() < RecordDistance)
				{
					RecordDistance = OB.Size();
					OtherBoid = ActorItr;
				}
			}
		}
		if (OtherBoid->IsValidLowLevel())
		{
			//Designate boid to follow
			//UKismetSystemLibrary::DrawDebugLine(GetWorld(), OtherBoid->GetActorLocation(), OtherBoid->GetActorLocation() + (OtherBoid->GetActorUpVector() * 75), FLinearColor::Blue, .07, 15);
			UKismetSystemLibrary::DrawDebugLine(GetWorld(), GetOwner()->GetActorLocation(), OtherBoid->GetActorLocation(), FLinearColor::Blue, .07, 5);

			FVector OB = OtherBoid->GetActorLocation() - GetOwner()->GetActorLocation();
			//Figure out if other boid is more right or left
			FVector A = OtherBoid->GetActorRightVector() * OB.GetSafeNormal();
			//Positive or Negative? | Figure out which way we need to move towards the other boid
			FVector Select = -(GetOwner()->GetActorRightVector());
			if (A.GetMax() > 0)
				Select = -Select;

			//Owner's wingtip location
			FVector OwnerWing = (Select * Wingspan) + GetOwner()->GetActorLocation();

			//Location of other boid's wing center
			FVector OBWingCenter = OtherBoid->GetActorRightVector() * Wingspan;

			//Draw owner's wing
			UKismetSystemLibrary::DrawDebugLine(GetWorld(), GetOwner()->GetActorLocation(), OwnerWing, FLinearColor::Yellow, .07, 15);

			//Location of other boid's wingtip at +50% Wingspan
			FVector OBWing = OtherBoid->GetActorLocation() + (OtherBoid->GetActorRightVector() * (Wingspan * 1.5));

			//Distance between Owner's wingtip and Other Boids wing center
			if ((OwnerWing - OBWingCenter).Size() < 100)
				return FVector();
			else
				return Arrive(OBWing, 100, Weight);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Failed to get OtherBoid"));
			//Return nothing if Other Boid isn't valid
			return FVector();
		}
	}
}

FVector UFlying_AC::Pursuit(AActor * Target, float LengthOfPrediction, bool Evade, float Weight)
{
	UFlying_AC* TargetComponent = nullptr;

	//Check if Target is valid
	if (Target->IsValidLowLevel())
	{
		//Check if Target has this component
		if (Target->GetComponentByClass(UFlying_AC::StaticClass()))
		{
			TargetComponent = Cast<UFlying_AC>(Target->GetComponentByClass(UFlying_AC::StaticClass()));
			//Seek out target using prediction to determine where target might go
			return Seek(Target->GetActorLocation() + TargetComponent->Velocity * (LengthOfPrediction / MaxSpeed), Evade, Weight);
		}
		else
			return FVector();
	}
	else
		return FVector();
}


/*Utilities*/


void UFlying_AC::GetNearbyBoids(TArray<FVector>& NormalScaledByDistance, FVector & Velocities, FVector & OtherLocations, float Distance)
{
	TArray<AActor*> FMCBoids = GetOtherFMCBoids();
	TArray<FVector> LDistances;
	FVector LVelocities;
	FVector LOtherLocations;

	//Iterate through all FMC Boids and find the return values
	for (AActor* ActorItr : FMCBoids)
	{
		FVector A = GetOwner()->GetActorLocation() - ActorItr->GetActorLocation();
		if (A.Size() > 0 && A.Size() < Distance)
		{
			//Set local  Boid Distances
			LDistances.Add(A.GetSafeNormal() / A.Size());

			//Set local Velocity
			LVelocities = Cast<UFlying_AC>(ActorItr->GetComponentByClass(UFlying_AC::StaticClass()))->Velocity + LVelocities;

			//Set Local Other Locations
			LOtherLocations = ActorItr->GetActorLocation() + LOtherLocations;
		}
	}

	NormalScaledByDistance = LDistances;
	Velocities = LVelocities;
	OtherLocations = LOtherLocations;
}

TArray<AActor*> UFlying_AC::GetOtherFMCBoids()
{
	TArray<AActor*> FMCBoids;
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AActor *FoundActor = *ActorItr;
		if (FoundActor->IsValidLowLevel() && FoundActor->GetComponentByClass(UFlying_AC::StaticClass()))
			FMCBoids.AddUnique(FoundActor);
	}
	return FMCBoids;
}

void UFlying_AC::ApplyForces(FVector Force, FVector& Change)
{
	Acceleration = Force + Acceleration;

	/*Set Velocity*/
	Velocity = Velocity + Acceleration;
	Velocity = Velocity.GetClampedToSize(0, MaxSpeed);

	/*Set Location*/
	FVector A = GetOwner()->GetActorLocation() + Velocity;
	Change = A;

	/*Set Rotation*/
	FRotator B;
	if (Velocity == FVector(0, 0, 0))
		B = GetOwner()->GetActorRotation();
	else
		B = UKismetMathLibrary::FindLookAtRotation(GetOwner()->GetActorLocation(), A);

	/*Set Owner Position*/
	GetOwner()->SetActorLocationAndRotation(A, B, true, nullptr, ETeleportType::None);

	/*Reset acceleration for next frame*/
	Acceleration = FVector(0, 0, 0);

	/*Debug Forces*/
	//FString OwnerLocation = GetOwner()->GetActorLocation().ToString();
	//UE_LOG(LogTemp, Warning, TEXT("Location: %s"), *OwnerLocation);
}

FVector UFlying_AC::GetNormalPoint(FVector Point, FVector LineStart, FVector LineEnd)
{
	return (LineEnd - LineStart) * FVector().DotProduct((Point - LineStart), (LineEnd - LineStart).GetSafeNormal()) + LineStart;
}

AActor* UFlying_AC::GetLeader()
{
	TArray<AActor*> FMCBoids = GetOtherFMCBoids();
	AActor* LLeader = nullptr;

	for (AActor* ActorItr : FMCBoids)
	{
		if (Cast<UFlying_AC>(ActorItr->GetComponentByClass(UFlying_AC::StaticClass()))->Leader)
			LLeader = ActorItr;
	}
	return LLeader;
}


/*Natural Forces*/


FVector UFlying_AC::Current(FVector Current, float Weight)
{
	return (Current.GetSafeNormal() * (MaxSpeed / 2) - Velocity).GetClampedToSize(0, MaxForce) * Weight;
}


//Combined Behaviours//


FVector UFlying_AC::Flocking()
{
	return Alignment() + Separation(250, 1.5) + Cohesion();
}

FVector UFlying_AC::V_Formation()
{
	if (Leader)
		return AlignWingsToAirFlow() + Alignment(500, .5) + Separation(250, .5);
	else
		//If not leader, follow leader slightly
		return AlignWingsToAirFlow() + Alignment(500, .5) + Separation(250, .5) + Seek(GetLeader()->GetActorLocation(), false, .5);
}


/*Unimplemented Behaviours*/


FVector UFlying_AC::FollowFlowField(APerch_A* FlowField)
{
	if(!FlowField->IsValidLowLevel())
	return FVector();

	FVector Direction = FlowField->Lookup(GetOwner()->GetActorLocation());
	return ((Direction * MaxSpeed) - Velocity).GetClampedToSize(0, MaxForce);
}

FVector UFlying_AC::FollowPath(APerch_A* Path)
{
	//Sequence 1
	if (!DrawOnce)
	{
		UKismetSystemLibrary::K2_SetTimer(Path, "DrawPath", 2, true);
		DrawOnce = true;
	}

	//Sequence 2
	FVector PredictedLocation = FVector();
	FVector Target = FVector();
	float Record = 0;
	FVector NormalPoint = FVector();
	FVector Normal = FVector();

	PredictedLocation = (Velocity.GetSafeNormal() * 50) + GetOwner()->GetActorLocation();

	for (int i = 0; i < Path->CurrentPathEndPoints.Num(); i++)
	{
		//Set NormalPoint
		if (NormalPoint.X < Path->CurrentPathEndPoints[i].X || NormalPoint.X > Path->CurrentPathEndPoints[i + 1].X)
			NormalPoint = Path->CurrentPathEndPoints[i + 1];
		else
			NormalPoint = GetNormalPoint(PredictedLocation, Path->CurrentPathEndPoints[i], Path->CurrentPathEndPoints[i + 1]);

		//Set Target
		FVector LLeftStart;
		FVector LLeftEnd;
		FVector LMiddleStart;
		FVector LMiddleEnd;
		FVector LRightStart;
		FVector LRightEnd;
		Path->GetPathVectors(Path->GetActorLocation(), Path->RelativePathEnd, LLeftStart, LLeftEnd, LMiddleStart, LMiddleEnd, LRightStart, LRightEnd);
		Target = NormalPoint + ((LMiddleEnd - LMiddleStart).GetSafeNormal() * 10);

		//Set Normal
		if ((PredictedLocation - NormalPoint).Size() < Record)
		{
			Record = (PredictedLocation - NormalPoint).Size();
			Normal = NormalPoint;
		}
	}

	//If record is greater than path radius, steer
	if (Record > Path->Radius)
		return Seek(Target, false);
	else
		return FVector();
}

FVector UFlying_AC::ObstacleAvoidance(int32 MaxVisionAhead, float Weight)
{
	/*
	if !within 25 pixels of edge 
	Desired velocity = current velocity

	else
	Desired velocity = Direction away from edge
	*/

	FVector Direction = FVector();
	FHitResult LOutHit;
	TArray<TEnumAsByte<EObjectTypeQuery>> Objects;
	FVector LOrigin = FVector();
	FVector LBoxExtent = FVector();

	Objects.Add(EObjectTypeQuery::ObjectTypeQuery1);
	Objects.Add(EObjectTypeQuery::ObjectTypeQuery2);
	Objects.Add(EObjectTypeQuery::ObjectTypeQuery3);
	Objects.Add(EObjectTypeQuery::ObjectTypeQuery4);

	FVector OwnerFV = UKismetMathLibrary::GetForwardVector(GetOwner()->GetActorRotation());
	FVector OwnerUV = UKismetMathLibrary::GetUpVector(GetOwner()->GetActorRotation());
	FVector OwnerRV = UKismetMathLibrary::GetRightVector(GetOwner()->GetActorRotation());
	TArray<FVector> OwnerVectors;
	//Forward Up
	OwnerVectors.Add(OwnerFV + OwnerUV);
	//Forward Down
	OwnerVectors.Add(OwnerFV + -OwnerUV);
	//Forward Right
	OwnerVectors.Add(OwnerFV + OwnerRV);
	//Forward Left
	OwnerVectors.Add(OwnerFV + -OwnerRV);
	//Forward
	OwnerVectors.Add(OwnerFV);

	for (int i = 0; i < OwnerVectors.Num(); i++)
	{
		FVector OwnerLoc = GetOwner()->GetActorLocation();
		bool A = UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), OwnerLoc, OwnerLoc + (OwnerVectors[i] * MaxVisionAhead), Objects, false, TArray<AActor*>(), EDrawDebugTrace::Persistent, LOutHit, true);
		if (A)
		{
			LOutHit.Actor->GetActorBounds(true, LOrigin, LBoxExtent);
			Direction = ((((OwnerLoc + (OwnerFV * MaxVisionAhead)) - LOrigin).GetSafeNormal() * MaxVisionAhead).GetClampedToSize(0, 500) * Weight) + Direction;
		}
		else
		{
			return FVector();
		}
	}
	return Direction;
}

FVector UFlying_AC::Boundaries(int32 MaxVisionAhead, float Weight)
{
	FVector LStart = GetOwner()->GetActorLocation();
	FVector LEnd = GetOwner()->GetActorLocation() + (GetOwner()->GetActorForwardVector() * MaxVisionAhead);
	FHitResult LOutHit;
	TArray<TEnumAsByte<EObjectTypeQuery>> Objects;

	Objects.Add(EObjectTypeQuery::ObjectTypeQuery1);
	Objects.Add(EObjectTypeQuery::ObjectTypeQuery2);
	Objects.Add(EObjectTypeQuery::ObjectTypeQuery3);
	Objects.Add(EObjectTypeQuery::ObjectTypeQuery4);

	UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), LStart, LEnd, Objects, false, TArray<AActor*>(), EDrawDebugTrace::Persistent, LOutHit, true);
	return LOutHit.Normal.GetClampedToSize(0, MaxForce) * Weight;
}

FVector UFlying_AC::FlyInCircle(ERotationDirection MovementDirection, float Radius, float SegmentAngle)
{
	UKismetSystemLibrary::DrawDebugCircle(GetWorld(), GetOwner()->GetActorLocation(), Radius, 12, FLinearColor::Red, 1, 10, FVector(0, 1, 0), FVector(0, 0, 1), false);
	return FVector();
}


/*Default Functions*/


// Called when the game starts
void UFlying_AC::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UFlying_AC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}