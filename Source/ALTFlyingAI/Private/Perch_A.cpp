// Fill out your copyright notice in the Description page of Project Settings.

#include "Perch_A.h"


// Sets default values
APerch_A::APerch_A()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Sphere = CreateDefaultSubobject<USphereComponent>(FName("BaseSphere"));
	Sphere->RegisterComponentWithWorld(GetWorld());
	RootComponent = Sphere;

	Arrow = CreateDefaultSubobject<UArrowComponent>(FName("BaseArrow"));
	Arrow->SetupAttachment(RootComponent);
	Arrow->RegisterComponentWithWorld(GetWorld());
	Arrow->ArrowSize = .5;

	TextRender = CreateDefaultSubobject<UTextRenderComponent>(FName("TextRender"));
	TextRender->SetupAttachment(RootComponent);
	TextRender->RegisterComponentWithWorld(GetWorld());
	TextRender->SetWorldLocation(FVector(0, 0, 120));
	TextRender->WorldSize = 45;
	TextRender->Text = NSLOCTEXT("", "", "Free perch here!");

	Perch = CreateDefaultSubobject<UStaticMeshComponent>(FName("Perch"));
	Perch->SetupAttachment(RootComponent);
	Perch->RegisterComponentWithWorld(GetWorld());
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshRef(TEXT("/Engine/BasicShapes/Cube.Cube"));
	Perch->SetStaticMesh(MeshRef.Object);
}


/*Path*/


void APerch_A::GetPathVectors(FVector StartLocation, FVector RelativeEndLocation, FVector & LeftStart, FVector & LeftEnd, FVector & MiddleStart, FVector & MiddleEnd, FVector & RightStart, FVector & RightEnd)
{
	FVector Ly;
	FVector Lx;
	FVector LSelected;

	// X
	if (RelativeEndLocation.X > 0)
		Lx = FVector(0, Radius, 0);
	else
		Lx = FVector(Radius, 0, 0);
	// Y
	if (RelativeEndLocation.X > 0)
		Ly = FVector(Radius, 0, 0);
	else
		Ly = FVector(0, Radius, 0);

	if (RelativeEndLocation.X >= 0)
		LSelected = Lx;
	else
		LSelected = Ly;

	// Set Values
	LeftStart = StartLocation + LSelected;
	LeftEnd = (StartLocation + RelativeEndLocation) + LSelected;
	MiddleStart = StartLocation;
	MiddleEnd = StartLocation + RelativeEndLocation;
	RightStart = StartLocation + LSelected;
	RightEnd = (StartLocation + RelativeEndLocation) + LSelected;
}

void APerch_A::DrawPath()
{
	FVector LLeftStart;
	FVector LLeftEnd;
	FVector LMiddleStart;
	FVector LMiddleEnd;
	FVector LRightStart;
	FVector LRightEnd;
	FLinearColor PathDebugColor;

	GetPathVectors(this->GetActorLocation(), RelativePathEnd, LLeftStart, LLeftEnd, LMiddleStart, LMiddleEnd, LRightStart, LRightEnd);

	//Draw starting path
	UKismetSystemLibrary::DrawDebugLine(GetWorld(), LLeftStart, LLeftEnd, FLinearColor::Black, 2, 12);
	UKismetSystemLibrary::DrawDebugLine(GetWorld(), LMiddleStart, LMiddleEnd, FLinearColor::Gray, 3, 10);
	UKismetSystemLibrary::DrawDebugLine(GetWorld(), LRightStart, LRightEnd, FLinearColor::Black, 2, 12);

	//Add path extensions
	for (int i = 0; i < CurrentPathExtensionsRelatives.Num(); i++)
	{
		GetPathVectors(CurrentPathEndPoints[i], FVector(CurrentPathExtensionsRelatives[i].X, CurrentPathExtensionsRelatives[i].Y, 0), LLeftStart, LLeftEnd, LMiddleStart, LMiddleEnd, LRightStart, LRightEnd);

		//Select path debug color
		if ((i % 2 + 2) == 0)
			PathDebugColor = FLinearColor::Black;

		if ((i % 2 + 2) == 1)
			PathDebugColor = FLinearColor::White;

		if ((i % 2 + 2) == 2)
			PathDebugColor = FLinearColor::Red;

		if ((i % 2 + 2) == 3)
			PathDebugColor = FLinearColor::Blue;

		UKismetSystemLibrary::DrawDebugLine(GetWorld(), LLeftStart, LLeftEnd, PathDebugColor, 2, 7);
		UKismetSystemLibrary::DrawDebugLine(GetWorld(), LMiddleStart, LMiddleEnd, PathDebugColor, 3, 5);
		UKismetSystemLibrary::DrawDebugLine(GetWorld(), LRightStart, LRightEnd, PathDebugColor, 2, 7);
	}
}

void APerch_A::NewPath()
{
	FVector LMagnitude;
	int LIndex;
	FVector LLeftStart;
	FVector LLeftEnd;
	FVector LMiddleStart;
	FVector LMiddleEnd;
	FVector LRightStart;
	FVector LRightEnd;

	//Clear the current path
	CurrentPathExtensionsRelatives.Empty();

	//Clear the current path
	CurrentPathEndPoints.Empty();

	//Add starting path and extensions later
	CurrentPathEndPoints.Add(this->GetActorLocation() + RelativePathEnd);

	//Path End Point - Path Extension End Point
	for (int i = 0; i < PathExtensions - 1; i++)
	{
		LMagnitude = UKismetMathLibrary::RandomUnitVectorInEllipticalConeInDegrees(CurrentPathEndPoints[i], 90, 180) * UKismetMathLibrary::RandomFloatInRange(0, 5000);
		LIndex = CurrentPathExtensionsRelatives.AddUnique(FVector(LMagnitude.X, LMagnitude.Y, this->GetActorLocation().Z));
		GetPathVectors(CurrentPathEndPoints[i], CurrentPathExtensionsRelatives[LIndex], LLeftStart, LLeftEnd, LMiddleStart, LMiddleEnd, LRightStart, LRightEnd);

		//Add end point for this path
		CurrentPathEndPoints.Add(FVector(LMiddleEnd.X, LMiddleEnd.Y, this->GetActorLocation().Z));
	}
}


/*Uncategorized*/


void APerch_A::NewOccupant(AActor * NewOccupant)
{
	//If not valid set occupant to nullptr
	if (NewOccupant->IsValidLowLevel())
	{
		Occupant = NewOccupant;
		//Set self as Occupant's Perch
		UpdateText();
	}
	else
	{
		//Set occupant's current perch to nullptr
		Occupant = nullptr;
		UpdateText();
	}
}

void APerch_A::UpdateText()
{
	if (Occupant->IsValidLowLevel())
		TextRender->SetText(NSLOCTEXT("", "", "Free perch here!"));
	else
		TextRender->SetText(NSLOCTEXT("", "", "There is an occupant at this perch."));
}

FVector APerch_A::Lookup(FVector ActorLocation)
{
	float LColumn = 0;
	float LRow = 0;

	LColumn = UKismetMathLibrary::Clamp(ActorLocation.X / Spacing, 0, Columns - 1);
	LRow = UKismetMathLibrary::Clamp(ActorLocation.Y / Spacing, 0, Rows - 1);
	return FVector(LColumn, LRow, 0);
}

void GetRC(int ItemNumber, int Count, int& Row, int& Column)
{
	Row = ItemNumber / Count;
	Column = ItemNumber % Count;
}


/*Events*/


// Called when the game starts or when spawned
void APerch_A::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APerch_A::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called when actor constructs
void APerch_A::OnConstruction(const FTransform& Transform)
{
	//Flow Field Sequence
	if (UseFlowField)
	{
		UArrowComponent* TempArrow;
		FVector LMin;
		FVector LMax;
		Perch->GetLocalBounds(LMin, LMax);
		BoundsX = Spacing + (LMax.X - LMin.X);
		BoundsY = Spacing + (LMax.Y - LMin.Y);

		//Destroy old arrow components
		TArray<UActorComponent*> LComponents = this->GetComponentsByClass(UArrowComponent::StaticClass());
		for (int i = 0; i < LComponents.Num(); i++)
		{
			if (LComponents[i]->GetFName() != FName("BaseArrow"))
				LComponents[i]->DestroyComponent();
		}

		//Create new arrow components for flowfield
		for (int i = 0; i < (Rows * Columns) - 1; i++)
		{
			int Row = 0;
			int Column = 0;
			GetRC(i, Rows, Row, Column);
			FRotator LRandRot = UKismetMathLibrary::RandomRotator();

			TempArrow = NewObject<UArrowComponent>(this, UArrowComponent::StaticClass());
			if (TempArrow)
			{
				TempArrow->RegisterComponent();
				TempArrow->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
				TempArrow->SetHiddenInGame(false);
				TempArrow->SetRelativeTransform(FTransform(FRotator(0, LRandRot.Yaw, LRandRot.Roll), FVector(Row * BoundsX, Column * BoundsY, 0), FVector(1, 1, 1)));
			}
		}
	}

	//Path Sequence
	else if (UsePath)
	{
		NewPath();
		DrawPath();
	}
}